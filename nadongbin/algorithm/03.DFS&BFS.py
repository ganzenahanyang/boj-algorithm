# 탐색이란 많은 양의 데이터 중에서 원하는 데이터를 찾는 과정을 말합니다.

# 스택 자료구조
# 선입 후출, 입구와 출구가 동일한 형태
# 삽입 삭제의 연산을 사용

# 그냥 리스트를 쓰면 됨
stack = []

stack.append(5)  # 가장 밑에서부터 쌓임
stack.append(2)
stack.append(3)
stack.append(7)
stack.pop()
stack.append(1)
stack.append(4)
stack.pop()

print(stack[::-1])  # 최상단 원소부터 출력
print(stack)  # 최하단 원소부터 출력

# 큐 자료구조
# 선입선출 입구와 출구가 모두 뚫려있는 터널과 같은 형태로 시각화 할 수 있다.
from collections import deque

# 큐(Queue) 구현을 위해 deque 라이브러리 사용
queue = deque()

# 삽입 및 삭제
queue.append(5)
queue.append(2)
queue.append(3)
queue.append(7)
queue.popleft()
queue.append(1)
queue.append(4)
queue.popleft()

print(queue)  # 먼저 들어온 순서대로 출력
queue.reverse()  # 역순으로 바꾸기
print(queue)  # 나중에 들어온 원소부터 출력


# 재귀함수

# 재귀함수는 반드시 종료 조건을 명시해야한다.
# 종료 조건을 제대로 명시하지 않으면 함수가 무한히 호출될 수 있다.

def recursive_function(i):
    # 100번째 호출을 했을 때 종료되도록 종료 조건 명시
    if i == 100:
        return
    print(i, '번째 함수에서', i + 1, '번째 재귀함수를 호출합니다.')
    recursive_function(i + 1)
    print(i, '번 째 재귀함수를 종료합니다.')


recursive_function(1)


# 팩토리얼 구현 예제
# 반복적으로 구현한 n!
def factorial_iterative(n):
    result = 1
    # 1부터 n까지의 수를 차례대로 곱하기
    for i in range(1, n + 1):
        result *= i
    return result


# 재귀적으로 구현한 n!
def factorial_recursive(n):
    # n이 1인 경우 1을 반환
    if n <= 1:
        return 1
    # n! = n * (n - 1)!을 그대로 코드로 작성하기
    return n * factorial_recursive(n - 1)


# 각각의 방식으로 구현한 n! 출력
print('반복으로 구현 : {}'.format(factorial_iterative(5)))
print('재귀로 구현 : {}'.format(factorial_recursive(5)))


# 유클리드 호제법
# 두 개의 자연수에 대한 최대공약수를 구하는 방법
# 두 자연수 A, B에 대해 (A>B) A를 B로 나눈 나머지를 R이라고 하자
# 이 때 A와 B의 최대 공약수는 B와 R의 최대공약수와 같다
def gcd(a, b):
    if a % b == 0:
        return b
    else:
        return gcd(b, a % b)


print(gcd(192, 162))


# 재귀 함수 사용시 유의 사항
# 재귀 함수를 활용하면 간결하게 작성할 수 있으나, 이해하기 어려운 형태가 될 수 있다.
# 모든 재귀 함수는 반복문을 이용하여 동일한 기능을 구현할 수 있다.
# 재귀 함수와 반복문은 각자 유리한 경우가 있다.
# 스택을 사용해야할 대는 재귀함수를 사용하는 경우가 있다.

# DFS
# 깊이우선탐색으로 깊은 부분을 우선적으로 탐색하는 알고리즘
# 스택 혹은 재귀를 사용하며 동작은 아래와 같다
# 1. 탐색 시작 노드를 스택에 삽입하고 방문처리를 한다.
# 2-1. 스택의 최상단 노드에 방문하지 않은 인접한 노드가 하나라도 있으면, 그 노드를 스택에 넣고 방문처리한다.
# 2-2. 방문하지 않은 인접노드가 없으면 스택에서 최상단 노드를 꺼낸다.
# 3. 더 이상 2번을 수행할 수 없을 때까지 반복한다.
def dfs(graph, v, visited):
    # 현재 노드를 방문 처리
    visited[v] = True
    print(v, end=' ')
    # 현재 노드와 연결된 다른 노드를 재귀적으로 방문
    for i in graph[v]:
        if not visited[i]:
            dfs(graph, i, visited)


# 각 노드가 연결된 정보를 표현 (2차원 리스트)
graph = [
    [],
    [2, 3, 8],
    [1, 7],
    [1, 4, 5],
    [3, 5],
    [3, 4],
    [7],
    [2, 6, 8],
    [1, 7]
]

# 각 노드가 방문된 정보를 표현 (1차원 리스트)
visited = [False] * 9

# 정의된 DFS 함수 호출
dfs(graph, 1, visited)
print()

# BFS
# 너비 우선 탐색, 그래프에서 가까운 노드부터 탐색하는 알고리즘
# 큐 자료구조를 이용하며 아래의 순서로 동작한다.
# 1. 탐색 시작 노드를 큐에 삽입하고 방문 처리를 한다.
# 2. 큐에서 노드를 꺼낸 뒤에 해당 노드의 인접 노드 중에서 방문하지 않은 노드를 모두 큐에 삽입하고 방문 처리한다.
# 3. 2를 반복한다.

# from collections import deque
#
#
# def bfs(graph, start, visited):
#     # 큐 구현을 위해 deque 라이브러리 사용
#     queue = deque([start])
#     # 현재 노드를 방문 처리
#     visited[start] = True
#     # 큐가 빌 때까지 반복
#     while queue:
#         # 큐에서 하나의 원소를 뽑아 출력하기
#         v = queue.popleft()
#         print(v, end=' ')
#         # 아직 방문하지 않은 인접한 원소들을 큐에 삽입
#         for i in graph[v]:
#             if not visited[i]:
#                 queue.append(i)
#                 visited[i] = True
#
#
# graph = [
#     [],
#     [2, 3, 8],
#     [1, 7],
#     [1, 4, 5],
#     [3, 5],
#     [3, 4],
#     [7],
#     [2, 6, 8],
#     [1, 7]
# ]
#
# # 각 노드가 방문된 정보를 표현 (1차원 리스트)
# visited = [False] * 9
#
# bfs(graph, 1, visited)
# print()


# 음료수 얼려 먹기
# N * M 크기의 얼음 틀이 있습니다. 구멍이 있는 부분은 0, 칸막이는 1로 표시된다. 구멍이 뚫려있는 부분끼리 상하좌우 연결되어있는것으로 간주한다.
# 얼음 틀의 모양이 주어졌을 때 생성되는 총 아이스크림의 개수를 구하는 프로그램을 작성하시오

# 4 5
# 00110
# 00011
# 11111
# 00000

# DFS
# 특정 지점의 상하좌우를 살펴보고 값이 0이면서 방문하지 않은 곳을 방문한다
# 방문한 지점에서 다시 상하좌우를 방문하면서 연결된 모든 지점을 방문한다
# 방문하지 않은 지점을 카운트

# def dfs(x, y):
#     # 주어진 범위를 벗어나는 경우에는 즉시 종료
#     if x <= -1 or x >= n or y <= -1 or y >= m:
#         return False
#     # 현재 노드를 방문하지 않았다면
#     if graph[x][y] == 0:
#         # 해당 노드 방문 처리
#         graph[x][y] = 1
#         # 상, 하, 좌, 우의 위치들도 모두 재귀적으로 호출
#         dfs(x - 1, y)
#         dfs(x, y - 1)
#         dfs(x + 1, y)
#         dfs(x, y + 1)
#         return True # 첫 지점에서 DFS가 완료되었다 -> 모든 덩어리들을 찾았다.
#     return False
#
#
# n, m = map(int, input().split())
#
# graph = []
# for i in range(n):
#     graph.append(list(map(int, input()))) # 공백없는 문자열 형태를 받을 때
#
# # 모든 노드에 음료수 채우기
# result = 0
# for i in range(n):
#     for j in range(m):
#         # 현 위치에서 DFS 수행
#         if dfs(i, j):
#             result += 1
#
# print(result)

# 미로 탈출
# 동빈이는 N * M 크기의 직사각형 형태의 미로에 갇혔다. 미로에는 여러 괴물이 있어 이를 피해야한다.
# 동빈이의 위치는 (1, 1) 이며, 미로의 출구는 (N, M)의 위치에 존재하며 한 번에 한칸씩 이동할 수 있다.
# 이 때 괴물이 있는 부분은 0으로, 괴물이 없는 부분은 1로 표시되어 있다.
# 이 때 동빈이가 탈출하기 위해 움직여야하는 최소 칸의 갯수를 구하시오

# 5 6
# 101010
# 111111
# 000001
# 111111
# 111111
# N, M을 공백 기준으로 구분하여 입력받기


n, m = map(int, input().split())

# 2차원 리스트 맵정보
graph = []
for i in range(n):
    graph.append(list(map(int, input())))

# 방향 정의
dx = [-1, 1, 0, 0]
dy = [0, 0, -1, 1]


def bfs(x, y):
    # 큐 구현을 위해 deque 사용
    queue = deque()
    queue.append((x, y))
    # 큐가 빌 때까지 반복
    while queue:
        x, y = queue.popleft()
        # 현재 위치에서 4가지 방향으로의 위치 확인
        if x == n - 1 and y == m - 1:
            break
        for i in range(4):
            nx = x + dx[i]
            ny = y + dy[i]
            # 미로 찾기 공간을 벗어난 경우 무시
            if nx < 0 or nx >= n or ny < 0 or ny >= m:
                continue
            # 벽인 경우 무시
            if graph[nx][ny] == 0:
                continue
            # 해당 노드를 처음 방문하는 경우 최단 거리 기록
            if graph[nx][ny] == 1:
                graph[nx][ny] = graph[x][y] + 1
                queue.append((nx, ny))

                for j in range(n):
                    for k in range(m):
                        print(graph[j][k], end='\t')
                    print()
                print()

    # 가장 오른쪽 아래의 최단거리 반환
    return graph[n - 1][m - 1]


print(bfs(0, 0))
