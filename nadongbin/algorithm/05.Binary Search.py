# 순차 탐색 : 리스트 안에 있는 특정한 데이터를 찾기 위해 앞에서부터 데이터를 하나씩 확인하는 방법
# 이진 탐색 : "정렬되어 있는" 리스트에서 탐색 범위를 절반씩 좁혀가며 데이터를 탐색하는 방법 (시작점, 끝점, 중간점을 활용하여 범위를 설정한다)

# 시간 복잡도
# 단계마다 탐색 범위를 2로 나누는 것과 동일하므로 연산 횟수는 logN에 비례한다


# 10 7
# 1 3 5 7 9 11 13 15 17 19

# 이진 탐색 소스코드 구현 (재귀)
def binary_search(array, target, start, end):
    if start > end:
        return None
    mid = (start + end) // 2
    # 찾은 경우 중간점 인덱스 반환
    if array[mid] == target:
        return mid
    # 중간점의 값보다 찾고자 하는 값이 작은 경우 왼쪽 학인
    elif array[mid] > target:
        return binary_search(array, target, start, mid - 1)
    # 중간점의 값보다 찾고자 하는 값이 큰 경우 오른쪽 확인
    else:
        return binary_search(array, target, mid + 1, end)


# 이진 탐색 소스코드 구현(반복문)
def binary_search_loop(array, target, start, end):
    while start <= end:
        mid = (start + end) // 2
        # 찾은 경우 중간점 인덱스 변환
        if array[mid] == target:
            return mid
        # 중간점의 값보다 찾고자 하는 값이 작은 경우 왼쪽 확인
        elif array[mid] > target:
            end = mid - 1
        # 중간점의 값보다 찾고자 하는 값이 큰 경우 오른쪽 확인
        else:
            start = mid + 1
    return None


# n(원소의 개수)과 target(찾고자 하는 값) 을 입력 받기
n, target = list(map(int, input().split()))

# 전체 원소 입력 받기
array = list(map(int, input().split()))

# 이진 탐색 수행 결과 출력
result = binary_search(array, target, 0, n - 1)

if result == None:
    print("원소가 존재하지 않습니다")
else:
    print(result + 1)

from bisect import bisect_left, bisect_right

a = [1, 2, 4, 4, 8]
x = 4

print(bisect_left(a, x))
print(bisect_right(a, x))


# 파라메트릭 서치
# 최적화 문제를 결정 문제로 바꾸어 해결하는 기법 (특정한 조건을 만족하는 가장 알맞은 값을 빠르게 찾는 최적화 문제) -> 이진탐색으로 해결


# 문제
# 떡볶이 떡 만들기
# 떡을 절단기로 자르려고 한다. 높이(H)를 지정하면 줄지어진 떡을 한 번에 절단한다. 높이가 H보다 긴 떡은 H 위의 부분이 잘릴 것이고, 낮은 떡은 잘리지 않는다.
# 예를 들어 높이가 19, 14, 10 인 떡이 나란히 있을때 H를 15로 하고 떡을 자르면 15, 14, 10이 될 것이다.
# 잘린 떡은 차례로 4, 0, 0 이다. 손님은 4만큼의 길이를 가져간다
# 손님이 왔을때 요청한 총 길이가 M일 때 적어도 M만큼의 떡을 얻기 위해 절단기에 설정할 수 있는 높이의 최댓값을 구하는 프로그램을 작성하시오

# 첫번째 줄 -> N : 떡의 갯수 / M : 요청한 떡의 길이
# 두번째 줄 -> 떡의 개별 높이
# 4 6
# 19 15 10 17

# 문제 풀이
# 적절한 높이를 찾기 위해 높이(H)를 반복해서 조정한다
# 현재 이 높이로 자르면 족너을 만족할 수 있는가? 를 확인한 뒤에 조건의 만족 여부에 따라서 탐색 범위를 좁혀서 해결할 수 있따.
# 절단기는 0부터 10억 중에 하나이다

# step 1 시작점 : 0 끝점 : 19 중간점 : 9 -> H 로 설정 잘린 떡의 크기의 합 25 > 필요한 떡의 길이 6
# step 2 시작점 : 10 끝점 : 19 중간점 : 14 -> H로 설정 잘린 떡의 크기의 합 9 > 필요한 떡의 길이 6
# step 3 시작점 : 15 끝점 : 19 중간점 : 17 -> H로 설정 잘린 떡의 크기의 합 2 < 필요한 떡의 길이 6
# step 4 시작점 : 15 끝점 : 16 중간점 : 15 -> H로 설정 잘린 떡의 크기의 합 6 == 필요한 떡의 길이 6

# 중간점의 값은 시간이 지날수록 최적화 되는 값이기 때문에 얻을 수 있는 떡의 길이와 필요한 떡의 길이가 차이가 날 때마다 중간점의 값을 기록해가면 된다.

# 떡의 갯수(N)와 요청한 떡의 길이 (M)을 입력
n, m = list(map(int, input().split(' ')))
# 각 떡의 개별 높이 정보를 입력
array = list(map(int, input().split()))

# 이진 탐색을 위한 시작점과 끝점 설정
start = 0
end = max(array)

# 이진 탐색 수행(반복적)
result = 0
while start <= end:
    total = 0
    mid = (start + end) // 2
    for x in array:
        # 잘랐을 때의 떡의 양 계산
        if x > mid:
            total += x - mid
    # 떡의 양이 부족한 경우 더 많이 자르기 (왼쪽 부분 탐색)
    if total < m:
        end = mid - 1
    # 떡의 양이 충분한 경우 덜 자르기 (오른쪽 부분 탐색)
    else:
        result = mid # 최대한 덜 잘랐을때가 정답이므로, 여기에서 result에 기록
        start = mid + 1

# 정답 출력
print(result)