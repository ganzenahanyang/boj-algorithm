# 구현
# 풀이를 떠올리는 것은 쉽지만, 소스코드로 옮기기 어려운 문제를 지칭한다.
# 1. 알고리즘은 간단한데 코드가 지나칠 만큼 길어지는 문제
# 2. 실수 연산을 다루고, 특정 소수점 자리까지 출력해야 하는 문제
# 3. 문자열을 특정한 기준에 따라서 끊어 처리하는 문제
# 4. 적절한 라이브러리를 찾아서 사용해야하는 문제

# 방향벡터의 활용
dx = [0, -1, 0, 1]
dy = [1, 0, -1, 0]

# 현재 위치
x, y = 2, 2

for i in range(4):
    # 다음 위치
    nx = x + dx[i]
    ny = y + dy[i]
    print(nx, ny)

# 상하좌우
# 여행가 A는 N * N 크기의 정사각형 공간 위에 서있습니다. 이 공간은 1 * 1 크기의 정사각형으로 나누어져 있습니다. 가장 왼쪽 위 자표는 (1, 1)이며 가장 오른쪽 아래는 (N, N) 입니다.
# 여행가 A는 상하좌우로 이동할 수 있으며 시작 좌표는 (1,1) 입니다. 우리는 이동 계획서를 받았습니다.
# 계획서에는 L, R, U, D 로 적인 이동 계획이 적혀있습니다. N * N 크기를 벗어나는 움직임은 무시됩니다

# 5
# R R R U D D

# 3 4

# 입력받기
N = int(input())
x, y = 1, 1
plans = input().split()

# L, R, U, D에 따른 이동 방향
dx = [0, 0, -1, 1]
dy = [-1, 1, 0, 0]
move_types = ['L', 'R', 'U', 'D']

# 이동 계획을 하나씩 확인하기
for plan in plans:
    # 이동 후 좌표 구하기
    for i in range(len(move_types)):
        if plan == move_types[i]:
            nx = x + dx[i]
            ny = y + dy[i]
            break
    # 공간을 벗어난는 경우는 무시
    if nx < 1 or ny < 1 or nx > N or ny > N:
        continue
    # 이동 수행
    x, y = nx, ny
    print(x, y)

print(x, y)

# 시각
# 정수 N이 입력되면 00시 00분 00초부터 N시 59분 59초까지의 모든 시각 중에서 3이 하나라도 포함되는 모든 경우의 수를 구하는 프로그램을 작성하시오
# 세어야 하는 시각 -> 00시 00분 03초, 00시 13분 30초
# 세면 안 되는 시각 -> 00시 02분 55초, 01시 27분 45초

# 하나씩 모두 세서 풀면 된다.
# 하루는 86400 초이므로 경우의 수는 86400가지다
# 시간을 1씩 증가시켠서 3을 증가시키면 된다.
N = int(input())
count = 0
for h in range(N + 1):
    for m in range(60):
        for s in range(60):
            # 매 시각 안에 '3'이 포함되어 있다면 카운트 증가
            if '3' in str(h) + str(m) + str(s):
                count += 1

print(count)

# 왕실의 나이트
# input : a1
# output : 2

# 현재 나이트의 위치 입력 받기
input_data = input()
row = int(input_data[1])
col = int(ord(input_data[0])) - int(ord('a')) + 1 # ord = 문자의 아스키 코드를 리턴, 시작 알파벳인 a를 빼주고 1을 더해주었다.

# 나이트가 이동할 수 있는 8가지 방향 정의
steps = [(-2, -1), (-1, -2), (1, -2), (2, -1), (2, 1), (1, 2), (-1, 2), (-2, 1)]

# 8가지 방향에 대하여 각 위치로 이동이 가능한지 확인
result = 0
for step in steps:
    # 이동하고자 하는 위치 확인
    next_row = row + step[0]
    next_col = col + step[1]
    # 해당 위치로 이동이 가능하다면 카운트 증가
    if next_row < 1 or next_row > 8 or next_col < 1 or next_col > 8:
        continue
    ++result

print(result)

# 문자열 재정렬
# 알파벳 대문자와 숫자(0 ~ 9)로만 구성된 문자열이 입력으로 주어진다.
# 이 떄, 모든 알파벳을 오름차순으로 정렬하여 이어서 출력한 뒤에 모든 숫자를 더한 값을 이어서 출력한다.
# 예를 들어 K1KA5CB7 ABCKK13을 출력합니다.

# 해법
# 문자열이 입력되었을 때 문자를 하나씩 확인한다
# 1. 숫자인 경우 따로 합계를 계산합니다.
# 2. 알파벳은 경우 별도의 리스트에 저장합니다.
# 결과적으로 리스트에 저장된 알파벳을 정렬해 출력하고, 합계를 뒤에 붙여 출력하면 정답입니다.

data = input()
result = []
value = 0

# 문자를 하나씩 확인하며
for x in data:
    # 알파벳인 경우 결과 리스트에 삽입
    if x.isalpha(): # -> 알파벳인지 판별하는 함수
        result.append(x) # 일단 알파벳을 넣고 나중에 정렬 
    # 숫자는 따로 더하기
    else:
        value += int(x)

# 알파벳을 오름차순으로 정렬
result.sort()

# 숫자가 하나라도 존재하는 경우 가장 뒤에 삽입
if value != 0:
    result.append(str(value))

# 최종 결과 출력(리스트를 문자열로 변환하여 출력)
print(''.join(result))