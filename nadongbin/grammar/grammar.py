# 양의 정수
a = 1000
print(a)

# 음의 정수
a = -7
print(a)

# 양의 실수
a = 157.93
print(a)

# 음의 실수
a = -57.93
print(a)

# 소수부가 0일떄 0을 생략
a = 5.
print(a)

# 정수부가 0일때 0을 생략
a = -.7
print(a)