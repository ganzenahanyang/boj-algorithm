# 내장 함수
# sum
result = sum([1, 2, 3, 4, 5])
print(result)

# min(), max()
min_result = min(7, 3, 4, 2)
max_result = max(7, 3, 5, 2)
print(min_result, max_result)

# eval() - 수식으로 표현된 값을 계산
result = eval("(3+5)*7")
print(result)

# sorted()
result = sorted([9, 1, 8, 5, 4])
reverse_result = sorted([9, 1, 8, 5, 4], reverse=True)
print(result)
print(reverse_result)

# sorted with key
array = [('홍길동', 35), ('이순신', 75), ('아무개', 50)]
result = sorted(array, key=lambda x:x[1], reverse=True)
print(result)

# permutation, combination
from itertools import *

data = ['A', 'B', 'C']
result = list(permutations(data, 3))
print(result)

result = list(combinations(data, 2))
print(result)

# 중복 순열, 중복 조합 (같은 값을 쌍으로 가져도 된다는 의미 ex) AA BB CC)
result = list(product(data, repeat=2))
print(result)

result = list(combinations_with_replacement(data, 2))
print(result)

# Counter
from collections import *
counter = Counter(['red', 'blue', 'red', 'green', 'blue', 'blue'])

print(counter['blue'])
print(counter['green'])
print(dict(counter))

# itertools

# heapq

# bisect

# collections

# math

import math
#최소 공배수(LCM)
def lcm(a, b):
    return a * b // math.gcd(a, b)

a = 21
b = 14

print(math.gcd(21, 14)) # 최대 공약수(GCD) 계산
print(lcm(21, 14)) # 최소 공배수(LCM) 계산