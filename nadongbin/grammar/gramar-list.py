# 직접 데이터를 넣어 초기화
a = [1, 2, 3, 4, 6, 7]
print(a)

# 네 번째 원소만 출력
print(a[3])

# 크기가 N이고, 모든 값이 0인 1차원 리스트 초기화
n = 10
a = [0] * n
print(a)

a = [1, 2, 3, 5, 6, 7, 8, 9]

# 8번째 출력
print(a[7])
# 뒤에서 첫번째 출력
print(a[-1])
# 뒤에서 세번째 출력
print(a[-3])
# 네번째 값 변경
a[3] = 7
print(a)
# 두 번째 원소부터 네 번째 원소까지
print(a[1:4])
# 0부터 9까지의 수를 포함하는 리스트
array = [i for i in range(10)]
print(array)
# 0부터 19까지의 수 중에서 홀수만 포함하는 리스트
array = [i for i in range(19) if i % 2 == 1]
print(array)
# 1부터 9까지의 수들의 제곱 값을 포함하는 리스트
array = [i * i for i in range(1, 10)]
print(array)
# N * M 크기의 2차원 리스트 초기화
n = 4
m = 3
array = [[0] * m for _ in range(n)]
print(array)

array = [[0] * m] * n
print(array)

a = [4,3,2, 1]

# 리스트 원소 뒤집기
a.reverse()
print("원소 뒤집기 : ", a)

# 특정 인덱스에 데이터 추가
a.insert(2, 3)
print("인덱스 2에 3 추가 : ", a)

# 특정 값인 데이터 갯수 세기
print("값이 3인 데이터의 갯수 : ", a.count(3))

# 특정 값 데이터 삭제
a.remove(1)
print("값이 1인 데이터 삭제 : ", a)

# remove_list 에 포함되지 않은 값만을 저장
a = [1, 2, 3, 4, 5, 5, 5]
remove_set = {3, 5}
result = [i for i in a if i not in remove_set]
print(result)
