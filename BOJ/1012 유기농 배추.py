from collections import deque

T = int(input())
dirX = [0, 0, 1, -1]
dirY = [1, -1, 0, 0]
q = deque()


def BFS(y, x):
    tq = deque()
    tq.append([y, x])
    while tq:
        y, x = tq.popleft()

        for i in range(4):
            yy = y + dirY[i]
            xx = x + dirX[i]
            if 0 <= xx < M and 0 <= yy < N and not visited[yy][xx] and board[yy][xx] == 1:
                visited[yy][xx] = True
                tq.append([yy, xx])


for t in range(T):
    M, N, K = map(int, input().split())
    board = [[0] * M for _ in range(N)]
    visited = [[False] * M for _ in range(N)]
    worm = 0
    # 배추밭 상태 초기화
    for k in range(K):
        x, y = map(int, input().split())
        q.append([y, x])
        board[y][x] = 1

    while q:
        nowY, nowX = q.popleft()
        if visited[nowY][nowX]:
            continue
        BFS(nowY, nowX)
        worm += 1

    print(worm)
