from collections import deque


def jMove():
    tempQ = deque()

    while jQ:
        y, x = jQ.popleft()
        # 이동한 곳이 불이 된 경우 제외
        if board[y][x] == 'F':
            continue
        visit[y][x] = True
        for i in range(4):
            yy = y + dy[i]
            xx = x + dx[i]
            # 밖으로 나간 경우
            if yy < 0 or xx < 0 or yy >= R or xx >= C:
                return 1
            if board[yy][xx] == 'F' or board[yy][xx] == '#' or visit[yy][xx]:
                continue
            visit[yy][xx] = True
            board[yy][xx] = 'J'
            tempQ.append([yy, xx])

    # 사방이 막혀 이동하지 못한 경우
    if len(tempQ) == 0:
        return -1

    jQ.extend(tempQ)
    tempQ.clear()
    return 0


def fireMove():
    tempQ = deque()
    while fQ:
        y, x = fQ.popleft()
        for i in range(4):
            yy = y + dy[i]
            xx = x + dx[i]
            if yy < 0 or xx < 0 or yy >= R or xx >= C or board[yy][xx] == '#' or board[yy][xx] == 'F':
                continue
            board[yy][xx] = 'F'
            tempQ.append([yy, xx])

    fQ.extend(tempQ)
    tempQ.clear()

R, C = map(int, input().split())

board = [list(input().strip()) for _ in range(R)]
visit = [[False] * C for _ in range(R)]
dy = [0, 0, 1, -1]
dx = [1, -1, 0, 0]
jQ = deque()
fQ = deque()
count = 0
for i in range(R):
    for j in range(C):
        if board[i][j] == 'J':
            jQ.append([i, j])
        elif board[i][j] == 'F':
            fQ.append([i, j])

while True:
    count += 1
    status = jMove()
    if status == 1:
        print(count)
        exit()
    elif status == -1:
        print("IMPOSSIBLE")
        exit()
    else:
        fireMove()