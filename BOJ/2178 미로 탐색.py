N, M = map(int, input().split())

board = [list(map(int, input())) for _ in range(N)]
weight = [[0] * M for _ in range(N)]
visited = [[False] * M for _ in range(N)]

dx = [1, -1, 0, 0]
dy = [0, 0, 1, -1]

queue = [[0, 0]]
weight[0][0] = 1
visited[0][0] = True

while queue:
    x, y = queue.pop(0)

    for i in range(4):
        xx = x + dx[i]
        yy = y + dy[i]
        if xx < 0 or xx >= M or yy < 0 or yy >= N:
            continue
        if board[yy][xx] == 0 or visited[yy][xx] is True:
            continue
        weight[yy][xx] = weight[y][x] + 1
        visited[yy][xx] = True
        queue.append([xx, yy])

print(weight[N - 1][M - 1])
