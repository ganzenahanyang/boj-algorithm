n = int(input())

for i in range(0, n):
    m, words = input().split()
    result = ""
    for j in words:
        result += j * int(m)
    print(result)