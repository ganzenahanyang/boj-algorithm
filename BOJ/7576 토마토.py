# 초기 상태 다익은 경우 0
# 익은 토마토 1  안 익은 토마토 0 토마토 없는 칸 -1
from collections import deque

M, N = map(int, input().split())

arr = [list(map(int, input().split())) for _ in range(N)]
weight = [[0] * M for _ in range(N)]
minDay = 0
dirI = [0, 0, 1, -1]
dirJ = [1, -1, 0, 0]
q = deque()
printZero = True  # 처음부터 다 익은 토마토만 있을 경우

for y in range(N):
    for x in range(M):
        if arr[y][x] == 0:
            printZero = False
        elif arr[y][x] == 1:
            q.append([y, x])  # 익은 토마토는 Q에
            weight[y][x] = 1

if printZero:
    print(0)
    exit()

while q:
    i, j = q.popleft()

    for k in range(4):
        ii = i + dirI[k]
        jj = j + dirJ[k]
        if ii < 0 or jj < 0 or ii >= N or jj >= M or arr[ii][jj] != 0 or weight[ii][jj] != 0:
            continue
        weight[ii][jj] = weight[i][j] + 1
        if minDay < weight[ii][jj]:
            minDay = weight[ii][jj]
        arr[ii][jj] = 1
        q.append([ii, jj])

for y in arr:
    for x in y:
        if x == 0:
            print(-1)
            exit()

print(minDay - 1)
