from collections import deque

dirY = [0, 0, 1, -1, 1, 1, -1, -1]
dirX = [1, -1, 0, 0, 1, -1, -1, 1]


def BFS(y, x):
    tq = deque()
    tq.append([y, x])
    while tq:
        nowY, nowX = tq.popleft()
        visited[nowY][nowX] = True
        for i in range(8):
            yy = nowY + dirY[i]
            xx = nowX + dirX[i]
            if 0 <= xx < W and 0 <= yy < H and not visited[yy][xx] and board[yy][xx] == 1:
                visited[yy][xx] = True
                tq.append([yy, xx])


while True:
    W, H = map(int, input().split())

    if W == 0 and H == 0:
        exit()

    board = [list(map(int, input().split())) for _ in range(H)]
    visited = [[False] * W for _ in range(H)]
    count = 0
    q = deque()
    for i in range(H):
        for j in range(W):
            if board[i][j] == 1:
                q.append([i, j])

    while q:
        y, x = q.popleft()
        if visited[y][x]:
            continue
        BFS(y, x)
        count += 1

    print(count)